package com.zuitt.example;

import  java.util.*;

public class Collections {
    public static void main(String[] args){
        // Arrays are fixed-size
        int[] intArray = new int[5];
        System.out.println("Initial state of inArray");
        System.out.println(Arrays.toString(intArray));

        intArray[0] = 1;
        intArray[intArray.length-1] = 5;
        System.out.println("Updated Array");
        System.out.println(Arrays.toString(intArray));


        String[][] classroom = new String[3][3];

        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";

        classroom[1][0] = "Brandon";
        classroom[1][1] = "Junjun";
        classroom[1][2] = "Jobert";

        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        System.out.println("This are all the students");
        System.out.println(Arrays.deepToString(classroom));

        //ArrayList resizable
        System.out.println("---------------");

        ArrayList<String> students = new ArrayList<String>();
        System.out.println(students);

        students.add("John");
        students.add("Paul");

        System.out.println(students.size());

        System.out.println(students.get(0));
        System.out.println(students);

        students.set(1,"George");
        System.out.println(students);

        students.remove(1);
        System.out.println(students);

        students.clear();
        System.out.println(students);

        System.out.println("---------------");
        //HashMaps

        HashMap<String, String> job_position = new HashMap<String, String>();

        job_position.put("Brandon", "Student");
        job_position.put("Alice", "Dreamer");
        System.out.println(job_position);

        job_position.put("Brandon", "Tambay");

        System.out.println(job_position.get("Alice"));
        job_position.remove("Brandon");
        System.out.println(job_position);

        System.out.println(job_position.keySet());
        System.out.println(job_position.values());


    }
}
