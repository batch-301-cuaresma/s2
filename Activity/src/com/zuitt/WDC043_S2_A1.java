package com.zuitt;

import java.awt.*;
import java.util.Scanner;

public class WDC043_S2_A1 {
    public static void main(String[] args){
        int inputYear;
        int isLeapYear;
        Scanner year = new Scanner(System.in);
        System.out.println("Input year to be check if a leap year.");
        inputYear = year.nextInt();
        isLeapYear = inputYear % 4;
        if(isLeapYear == 0){
            System.out.println(inputYear + " is a Leap Year!");
        }
        else{
            System.out.println(inputYear + " is not a Leap Year!");
        }
    }
}
