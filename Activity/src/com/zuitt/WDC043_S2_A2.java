package com.zuitt;

import java.util.*;

public class WDC043_S2_A2 {
    public static void main(String[] args){
        System.out.println("Arrays");
        int[] intArray = new int[5];
        intArray[0]= 2;
        intArray[1]= 3;
        intArray[2]= 5;
        intArray[3]= 7;
        intArray[4]= 11;

        System.out.println("The first prime number is: " + intArray[0]);
        System.out.println("The second prime number is: " + intArray[1]);
        System.out.println("The third prime number is: " + intArray[2]);
        System.out.println("The fourth prime number is: " + intArray[3]);
        System.out.println("The fifth prime number is: " + intArray[4]);

        System.out.println("---------------------------");
        System.out.println("ArrayList");

        ArrayList<String>  friends = new ArrayList<String>();
        friends.add("alpha");
        friends.add("joy");
        friends.add("jen");
        friends.add("pia");

        System.out.println("My friends are: " + friends);

        System.out.println("---------------------------");
        System.out.println("HashMap");

        HashMap<String, Integer> items = new HashMap<String, Integer>();

        items.put("toothpaste", 15);
        items.put("toothbrush", 20);
        items.put("soap", 12);

        System.out.println("Our current inventory consists of:  " + items);


    }
}
